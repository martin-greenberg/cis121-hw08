import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class JavaScriptPointsWriter {

    /**
     * Writes the Points out to a file in JavaScript syntax.
     * The filename is decided based on the second integer argument.
     */
    public static void writeJSPoints(List<Point> list, int fileNum) {
        FileWriter out;
        String filename = "points" + fileNum + ".js";
        try {
            out = new FileWriter(new File(filename));

            // Write heading to js file
            out.write("//Javascript file\n");
            out.write("//data structure for shortest path\n");
            out.write("var points" + fileNum + " = new Array();\n");

            // Write the points
            int count = 0;
            for (Point p : list) {
                out.write("points" + fileNum + "[" + count + "] =" + p.getX()
                          + ";\n");
                count++;
                out.write("points" + fileNum + "[" + count + "] =" + p.getY()
                          + ";\n");
                count++;
            }
            out.close();
        } catch (IOException e) {
            throw new RuntimeException("cannot write file: " + filename);
        }
    }
}
