import java.util.NoSuchElementException;
import java.util.PriorityQueue;


public class PQAdapter<K extends Comparable<? super K>> extends PriorityQueue<K> implements BinaryMinHeapI<K>{
	
	public boolean isEmpty(){
		return (this.size() == 0);
	}

    /**
     * Returns the least element in the heap
     * @throws NoSuchElementException
     *         - if the heap is empty
     */
	
	@Override
	public K removeMin() {
		if(this.isEmpty()){
			throw new NoSuchElementException();
		} else {
			return this.poll();
		}
	}
	
	@Override
	public void insert(K element) {
		if(element == null){
			throw new NullPointerException();
		} else {
			this.offer(element);
		}
	}

	@Override
	public void updateKey(K oldElem, K newElem) {
		if(!(this.contains(oldElem))){
			throw new NoSuchElementException();
		}
		if((oldElem==null)||(newElem==null)){
			throw new NullPointerException();
		}
		this.remove(oldElem);
		this.offer(newElem);
	}
}
