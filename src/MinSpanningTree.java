import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MinSpanningTree {
	static HashMap<Point, Vertex> vertexMap = new HashMap<Point,Vertex>();
	
	static class Vertex implements Comparable<Vertex>{
    	private List<Street> neighbors;
    	private Point pointObj;
    	private boolean marked;
    	private Street streetTo;
    	private double distTo;
    	Vertex(Point p, Map<Point, List<Street>> map){
    		if(p==null){
    			throw new IllegalArgumentException();
    		}
    		neighbors = map.get(p);
    		pointObj = p;
    		distTo = Double.POSITIVE_INFINITY;
    		marked = false;
    	}
    	
    	public double getDistTo(){
    		return this.distTo;
    	}
    	
    	public void setDistTo(double d){
    		this.distTo = d;
    	}
    	
    	public boolean getMarked(){
    		return this.marked;
    	}
    	
    	public void setMarked(boolean b){
    		this.marked = b;
    	}
    	
    	public void setStreetTo(Street s){
    		this.streetTo = s;
    	}
    	
    	public Street getStreetTo(){
    		return this.streetTo;
    	}
    	
    	public List<Street> getNeighbors(){
    		return this.neighbors;
    	}
    	
    	public Point getPoint(){
    		return this.pointObj;
    	}

		@Override
		public int compareTo(Vertex o) {
			return (int) (this.distTo - o.distTo);
		}
	}
	
	static PQAdapter<Vertex> pq = new PQAdapter<Vertex>();
	
    /**
     * Computes the minimum spanning tree using EAGER Prim's algorithm for the
     * intersections that have been generated. Implementation should run in
     * O(E*log(V)) or faster. Runtime: all steps run in O(E log V).
     *
     * @throws IllegalArgumentException
     *             - if argument map is null
     *             - if any element of the map (vertex or edge) is null
     *             - if graph is not connected
     */
    public static List<Street> minST(Map<Point, List<Street>> map) {
    	if(map == null){
        	throw new IllegalArgumentException();
    	}
    	
    	ArrayList<Street> output = new ArrayList<Street>();
    	Point[] points = map.keySet().toArray(new Point[map.size()]);
    	for(Point p : points){
    		vertexMap.put(p, new Vertex(p, map));
    	}
    	
    	Vertex start = vertexMap.get(points[0]);
    	start.setDistTo(0);
    	pq.insert(start);
    	while(!pq.isEmpty()){
    		Vertex toVisit = pq.removeMin();
    		if(toVisit.getDistTo() == Double.POSITIVE_INFINITY){
    			throw new IllegalArgumentException(); //graph is not connected
    		}
    		Street MSTedge = toVisit.getStreetTo();
    		if(MSTedge != null){
    			output.add(MSTedge);
    		}
    		if(map == null){
    			System.out.println("map is null");
    		}
    		visit(map, toVisit);
    	}
    	return output;
	}
    
    private static Vertex getOtherVertex(Street s, Vertex v){
    	Point p = v.getPoint();
    	if((s==null)||(s.getFirstPoint()==null)||(s.getSecondPoint()==null)){
    		throw new IllegalArgumentException();
    	}
    	if(s.getFirstPoint().equals(p)){
    		return vertexMap.get(s.getSecondPoint());
    	}
    	if(s.getSecondPoint().equals(p)){
    		return vertexMap.get(s.getFirstPoint());
    	}
    	throw new IllegalArgumentException(); //if we get here then p is not on s
    }
	private static void visit(Map<Point, List<Street>> map, Vertex v){
		v.setMarked(true);
		for(Street s : v.getNeighbors()){
			Vertex other = getOtherVertex(s,v);
			if(other.getMarked()){
				continue;
			}
			if(s.getDistance() < other.getDistTo()){
				other.setStreetTo(s);
				other.setDistTo(s.getDistance());
				if(pq.contains(other)){
					pq.updateKey(other, other); //
				} else {
					pq.insert(other);
				}
			}
		}
	}
}
