import java.io.*;
import java.util.*;

/**
 * This class ties together the previous problems by going from
 * beginning (reading the file) to the end (visualizing the relevant
 * points and their edges).
 *
 * @author CIS-121 Staff
 */
public class MapMaker {

    static Map<Point,List<Street>> graph;
    static Point start,end;

    /**
     * This method should be able to read the data file, provide a graph
     * determine the ShortestPath and MinimumSpanningTrees for that graph.
     * It should also pass points to JavaScriptPointsWriter.java to generate
     * a visual representation of the tree or path.
     *
     * @throws IOException
     *           -if the input file is of the incorrect format when being parsed
     */
    public static void main(String[] args) throws IOException{
        GraphMaker grMaker = new GraphMaker();

        // Parses data file
        graph = grMaker.parse("penn.txt");

        // Passes start and end points to shortestPath
        start = grMaker.getStartPoint();
        end = grMaker.getEndPoint();

        // Problem 1: calculate shortest path
        List<Point> path = ShortestPath.getPath(graph, start, end);
        JavaScriptPointsWriter.writeJSPoints(path, 1);

        // Problem 2: minimum spanning tree
        List<Street> mst = MinSpanningTree.minST(graph);
        List<Point> pts = new LinkedList<Point>();
        for (Street s : mst){
            pts.add(s.getFirstPoint());
            pts.add(s.getSecondPoint());
        }
        JavaScriptPointsWriter.writeJSPoints(pts, 2);
    }
}
