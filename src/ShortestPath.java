import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class ShortestPath {

    /**
     * Calculates the shortest path between two points via Dijkstra's
     * algorithm. Returns a list of points that forms the path, in that order.
     * The list has the source of the path at the head (first element) and the
     * destination at the tail (last element).
     *
     * @throws IllegalArgumentException
     *             - if any of the arguments graph, a, or b are null
     *             - if any component (vertex or edge) of the graph is null
     *             - if locations of a and/or b are NOT part of the graph
     *             - if there is no path between a and b
     */
    public static List<Point> getPath(Map<Point, List<Street>> graph,
                                          Point a, Point b) {
        if((a==null)||(b==null)||(graph==null) || (!graph.containsKey(a)) || (!graph.containsKey(b))){
        	throw new IllegalArgumentException();
        }
        final Map<Point,List<Street>> graphInner = graph;
        class Vertex implements Comparable<Vertex>{
        	private double distance;
        	private List<Street> neighbors;
        	private Point pointObj;
        	private Point prev;
        	Vertex(Point p){
        		if(p==null){
        			throw new IllegalArgumentException();
        		}
        		prev = null;
        		distance = Double.POSITIVE_INFINITY;
        		neighbors = graphInner.get(p);
        		pointObj = p;
        	}
        	
        	public double getDistance(){
        		return this.distance;
        	}
        	
        	public void setDistance(double d){
        		this.distance = d;
        	}
        	
        	public List<Street> getNeighbors(){
        		return this.neighbors;
        	}
        	
        	public void setPrev(Point p){
        		this.prev = p;
        	}
        	
        	public Point getPrev(){
        		return this.prev;
        	}
        	
        	public Point getPoint(){
        		return this.pointObj;
        	}
        	public Street streetBtn(Vertex v){ //returns the street if an street exists between this vertex and v
        		for(Street s : this.neighbors){
        			if (s == null){
        				throw new IllegalArgumentException();
        			}
        			if(s.getFirstPoint().equals(this.pointObj) && 
        					s.getSecondPoint().equals(v.pointObj)){
        				return s;
        			}
        			if(s.getSecondPoint().equals(this.pointObj) && 
        					s.getFirstPoint().equals(v.pointObj)){
        				return s;
        			}
        		}
        		return null;
        	}

			@Override
			public int compareTo(Vertex o) {
				return (int) (this.distance - o.distance);
			}
        }
        
        HashSet<Point> visited = new HashSet<Point>(); //set of visited vertices
        HashMap<Point, Vertex> vertexMap = new HashMap<Point,Vertex>();
        PQAdapter<Vertex> vertices = new PQAdapter<Vertex>(); //create set of vertices
        for(Point p : graph.keySet()){
        	Vertex newV = new Vertex(p); //will throw IllegalArgumentException if p is null
        	if(p.equals(a)){
        		newV.setDistance(0);
        	}
        	vertexMap.put(p, newV);
        	vertices.insert(newV); //add all vertices to the set with distance = infinity except for a which has dist=0
        }
        while(!vertices.isEmpty()){
        	Vertex u = vertices.removeMin();
        	visited.add(u.pointObj);
        	if(u.getDistance() == Double.POSITIVE_INFINITY){
        		throw new IllegalArgumentException(); //if we get here then no a~b path exists so throw exception
        	}
        	List<Street> neighbors = u.getNeighbors();
        	PQAdapter<Vertex> neighborPQ = new PQAdapter<Vertex>();
        	for(Street s : neighbors){
        		neighborPQ.add(vertexMap.get(s.getSecondPoint())); //build neighborPQ
        	}
        	while(!neighborPQ.isEmpty()){
    			Vertex v = neighborPQ.removeMin();
    			if(!visited.contains(v.pointObj)){
	        		double candidate_dist = u.getDistance() + u.streetBtn(v).getDistance();
	        		double current_dist = v.getDistance();
	        		if(candidate_dist < current_dist){ //if the a~v path through u has lower weight, relax v
	        			Vertex newElem = new Vertex(v.pointObj);
	        			newElem.setPrev(u.getPoint());
	        			newElem.setDistance(candidate_dist);
	        			vertices.updateKey(v, newElem);
	        			vertexMap.put(v.pointObj, newElem); //update vertex object in vertex map
    				}
    			}
        	}
        	if(u.pointObj.equals(b)){
        		break; //once b is visited, algorithm can deliver shortest a~b path
        	}
        }
        ArrayList<Point> output = new ArrayList<Point>();
        Point toAdd = b;
        while(toAdd != null){
        	output.add(toAdd);
        	toAdd = vertexMap.get(toAdd).getPrev();
        }
        Collections.reverse(output);
        return output;
    }
}
