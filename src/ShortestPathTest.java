import static org.junit.Assert.*;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class ShortestPathTest {
	Map<Point, List<Street>> graph;
	Point a;
	Point b;
	
	@Before
	public void setUp() throws Exception {
		GraphMaker maker = new GraphMaker();
		graph = maker.parse("penn.txt");
		a = maker.getStartPoint();
		b = maker.getEndPoint();
	}

	@Test
	public void testGetPath() {
		System.out.println(ShortestPath.getPath(graph, a, b));
	}

}
