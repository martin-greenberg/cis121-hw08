import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class MinSpanningTreeTest {
	Map<Point, List<Street>> graph;
	
	@Before
	public void setUp() throws Exception {
		GraphMaker maker = new GraphMaker();
		graph = maker.parse("penn.txt");
	}

	@Test
	public void test() {
		List<Street> out = MinSpanningTree.minST(graph);
		Iterator<Street> iter = out.iterator();
		int i = 1;
		while(iter.hasNext()){
			System.out.print("Street "+i+": ");
			System.out.println(iter.next().getId());
			i++;
		}
		System.out.println("done");
	}

}
